import fenics as fe
import matplotlib.pyplot as plt
import math
import numpy as np


def local_project(fce, space):
	lp_trial, lp_test = fe.TrialFunction(space), fe.TestFunction(space)
	lp_a = fe.inner(lp_trial, lp_test)*fe.dx
	lp_L = fe.inner(fce, lp_test)*fe.dx
	local_solver = fe.LocalSolver(lp_a, lp_L)
	local_solver.factorize()
	lp_f = fe.Function(space)
	local_solver.solve_local_rhs(lp_f)
	return lp_f


# --------------------
# Classes and methods
# --------------------
class Top(fe.SubDomain):
	def inside(self, x, on_boundary):
		tol = 1e-10
		return abs(x[1] - 1) < tol and on_boundary


class Bottom(fe.SubDomain):
	def inside(self, x, on_boundary):
		tol = 1e-10
		return abs(x[1]) < tol and on_boundary


def epsilon(a):
	return fe.sym(fe.grad(a))


def sigma(a):
	return lam * fe.tr(epsilon(a)) * fe.Identity(d) + 2.0 * mu * epsilon(a)


def sigma_ten(a):
	return local_project(lam * fe.tr(fe.grad(a)) * fe.Identity(d) + 2.0 * mu * epsilon(a), Ten)


# --------------------
# Define geometry
# --------------------
# mesh = UnitCubeMesh(5, 5, 5)
mesh = fe.UnitCubeMesh.create(5,5,5,fe.CellType.Type.hexahedron)

# Hexahedrons do not support plotting
# plot(mesh, "Mesh")
# plt.show()

# --------------------
# Define spaces
# --------------------
W = fe.VectorFunctionSpace(mesh, 'CG', 1)
Ten = fe.TensorFunctionSpace(mesh, 'DG', 0)
dd_u = fe.TrialFunction(W)
v = fe.TestFunction(W)

# --------------------
# Parameters
# --------------------
start_time = 0.0
end_time = 30.0
tau = 1.0e-1  # time increment

# Dimension of task
d = 3

# Material parameters
n = 22
Ginf = 682.18e3
Gs = 1.0e3 * np.array(
	[6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1,
	 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
Ts = np.array(
	[1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
	 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
eta = np.zeros(n)
for k in range(0, n):
	eta[k] = Ts[k]*Gs[k]

rho = 1.0e6
nu = 0.2

G_unit = 1.0
lam, mu = (2 * G_unit * nu / (1 - 2 * nu), G_unit)

# Other constants
A = []
B = []
C = []
sumA = 0.0
sumB = 0.0
sumC = 0.0

for k in range(0, n):
	A.append(math.exp(-Gs[k] / eta[k] * tau))
	sumA += A[k]
	B.append(-eta[k] * math.expm1(-Gs[k] / eta[k] * tau))
	sumB += B[k]
	C.append(0.5 * eta[k] * (tau + eta[k] / Gs[k] * math.expm1(-Gs[k] / eta[k] * tau)))
	sumC += C[k]

# --------------------
# Boundary conditions
# --------------------
# Instants of classes top and bottom
top = Top()
bottom = Bottom()

# Label top boundary as ds(1)
boundaries = fe.MeshFunction("size_t", mesh, mesh.topology().dim() - 1)
boundaries.set_all(0)
top.mark(boundaries, 1)
ds = fe.Measure('ds', subdomain_data=boundaries)

# Dirichlet boundary condition for bottom
DB_bottom = fe.DirichletBC(W, fe.Constant((0.0, 0.0, 0.0)), bottom)

# --------------------
# Initialization
# --------------------
u = fe.Function(W)
d_u = fe.Function(W)
dd_u_ = fe.Function(W)
dd_u_old = fe.Function(W)
f_v = [fe.Function(Ten) for i in range(n)]

t = start_time

ff = fe.Expression(("0.0", "1.0e6", "0.0"), degree=0)

# --------------------
# XDMF output
# --------------------
# Create XDMF files for visualization output
xdmffile_u = fe.XDMFFile('Results_v3_n7/disp.xdmf')
xdmffile_ddu = fe.XDMFFile('Results_v3_n7/ddu.xdmf')

# --------------------
# Initial condition
# --------------------
LHS0 = rho * fe.dot(v, dd_u) * fe.dx
RHS0 = fe.dot(v, ff) * ds(1)
fe.solve(LHS0 == RHS0, dd_u_old, [DB_bottom])

# --------------------
# Variational form
# --------------------
LHS = rho*fe.dot(v, dd_u)*fe.dx + fe.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u))*fe.dx
RHS = -fe.inner(epsilon(v), Ginf*sigma(u))*fe.dx - fe.inner(epsilon(v), (Ginf*tau + sumB)*sigma(d_u))*fe.dx - \
		fe.inner(epsilon(v), (0.25*Ginf*tau*tau + sumC)*sigma(dd_u_old))*fe.dx + fe.dot(v, ff) * ds(1)
RHS -= fe.inner(epsilon(v), sum(A[k]*f_v[k] for k in range(0, n)))*fe.dx

# --------------------
# Main load loop
# --------------------
while t <= end_time:

	# Solve linear variational problem
	fe.solve(LHS == RHS, dd_u_, [DB_bottom])

	# Update of rest quantities
	temp1 = sigma_ten(d_u)
	temp2 = sigma_ten(dd_u_)
	temp3 = sigma_ten(dd_u_old)
	for k in range(0, n):
		f_v[k].assign(A[k]*f_v[k] + B[k]*temp1 + C[k]*temp2 + C[k]*temp3)

	u.assign(u + d_u*tau + 0.25*(dd_u_old + dd_u_)*tau*tau)
	d_u.assign(d_u + 0.5 * (dd_u_old + dd_u_) * tau)
	dd_u_old.assign(dd_u_)

	# Save solution to file (XDMF/HDF5)
	xdmffile_u.write(u, t)
	xdmffile_ddu.write(dd_u_, t)

	# Increment of time
	t = t + tau

xdmffile_u.close()
xdmffile_ddu.close()
