import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import math
from scipy.integrate import odeint

# plt.rc('text', usetex=True)
# plt.rc('font', family='serif')

# Style of graphs
plt.style.use("classic")
mpl.rc('image', cmap='gray')


# Definition of problem for ODE solver (step load)
def maxwell_step(y, t):
	dydt = [y[1]]
	y_temp = y[2:]
	dydt.append(1.0/m*(1.0e6*np.heaviside(t, 0) - sum(y_temp) - Ginf*y[0]))
	for i in range(0, n):
		dydt.append(y[1]*Gs[i] - y_temp[i]*Gs[i] / eta[i])
	return dydt


# Definition of problem for ODE solver (sin load)
def maxwell_sin(y, t):
	dydt = [y[1]]
	y_temp = y[2:]
	dydt.append(1.0/m*(1.0e6*math.sin(t + 0.1) - sum(y_temp) - Ginf*y[0]))
	for i in range(0, n):
		dydt.append(y[1]*Gs[i] - y_temp[i]*Gs[i]/eta[i])
	return dydt


# --------------------
# Quasi exact solution
# --------------------
def quasi_exact_solution(a, t_s, t_e):
	# Initial values
	y0 = [0]*(n+2)

	# Time discretization
	t = np.linspace(t_s, t_e, 1000)
	# Solution
	if (a == 0):
		sol = odeint(maxwell_step, y0, t, args=())
	elif (a == 1):
		sol = odeint(maxwell_sin, y0, t, args=())

	return t, sol


# ----------------
# Newmark solution
# ----------------
def newmark_solution(F, steps, dt):
	# Initiation
	fv = np.zeros((steps, n))
	r = np.zeros(steps)
	dr = np.zeros(steps)
	ddr = np.zeros(steps)
	t = np.zeros(steps)
	Ei = np.zeros(steps)
	EF = np.zeros(steps)
	D = np.zeros(steps)
	D_apr = np.zeros(steps)
	Ec = np.zeros(steps)

	# Pre-calculated values
	summ1 = 0.0
	summ2 = 0.0
	for k in range(0, n):
		summ1 = summ1 + 0.5 * eta[k] * (dt - eta[k] / Gs[k] * (1.0 - math.exp(-Gs[k] / eta[k] * dt)))
	for k in range(0, n):
		summ2 = summ2 + eta[k] * (1.0 - math.exp(-Gs[k] / eta[k] * dt))

	# Solution
	ddr[0] = F[0] / m
	for i in range(1, steps):

		# Auxiliary scalar
		summ3 = 0.0
		for k in range(0, n):
			summ3 = summ3 + fv[i - 1][k] * math.exp(-Gs[k] / eta[k] * dt)

		den = m + 0.25 * Ginf * dt * dt + summ1
		nom = F[i] - summ3 - Ginf * r[i - 1] - (Ginf * dt + summ2) * dr[i - 1] - (0.25 * Ginf * dt * dt + summ1) * ddr[
			i - 1]

		# updating
		t[i] = t_start + i * dt
		ddr[i] = nom / den
		dr[i] = dr[i - 1] + 0.5 * (ddr[i - 1] + ddr[i]) * dt
		r[i] = r[i - 1] + dr[i - 1] * dt + 0.25 * (ddr[i - 1] + ddr[i]) * dt * dt

		# Viscous forces updating
		for k in range(0, n):
			fv[i][k] = fv[i - 1][k] * math.exp(-Gs[k] / eta[k] * dt) + eta[k] * (1.0 - math.exp(-Gs[k] / eta[k] * dt)) * \
					   dr[i - 1] + 0.5 * eta[k] * (dt - eta[k] / Gs[k] * (1.0 - math.exp(-Gs[k] / eta[k] * dt))) * (
								   ddr[i] + ddr[i - 1])

	return t, r, dr, ddr, fv


def calc_energies(t, dt, r, dr, ddr, fv, F):
	Ei = np.zeros(steps)
	EF = np.zeros(steps)
	D_apr = np.zeros(steps)
	for i in range(1, len(t)):
		# Energy of system
		temp1 = 0.0
		temp2 = 0.0
		for k in range(0, n):
			temp1 = temp1 + 0.5 * (fv[i][k]) ** 2 / Gs[k]
			temp2 = temp2 + (0.5 * fv[i][k] ** 2 + 0.5 * fv[i - 1][k] ** 2) / eta[k] * dt
			C = fv[i - 1][k] - eta[k] * dr[i - 1] + 0.5 * eta[k] * eta[k] * (ddr[i] + ddr[i - 1]) / Gs[k]
			A = 0.5 * eta[k] * (ddr[i] + ddr[i - 1])
			B = eta[k] * dr[i - 1] - 0.5 * eta[k] * eta[k] * (ddr[i] + ddr[i - 1]) / Gs[k]

		D_apr[i] = D_apr[i - 1] + temp2
		Ei[i] = 0.5 * m * (dr[i]) ** 2 + 0.5 * Ginf * (r[i]) ** 2 + temp1
		EF[i] = EF[i - 1] + 0.5 * (F[i] * dr[i] + F[i - 1] * dr[i - 1]) * dt
	# Ec[i] = Ei[i] + D_apr[i] - EF[i]
	return Ei, EF, D_apr


def make_comparison_plot(t1, r1, t2, r2, fn, fn2):
	f, fig = plt.subplots(figsize=(6, 4.5))
	plt.plot(t1, r1[:, 0], "--", c="black", label=r"Reference")
	plt.plot(t2, r2, "black", label=r"Newmark")
	plt.xlabel(r"time $t$ [s]")
	plt.ylabel(r"displacement $r(t)$ [m]")
	plt.legend(loc=4)
	# plt.show()
	plt.tight_layout()
	f.savefig("%s_t%s.pdf" % (fn, fn2))


def make_energy_plot(t, Ei, EF, D_apr, fn, fn2):
	f, fig = plt.subplots(figsize=(6, 4.5))
	plt.plot(t, Ei / EF, c="black", label=r"$\mathcal{E}_\mathrm{int}/\mathcal{W}_\mathrm{d}$")
	# plt.plot(t, EF / EF, '--', c="black", label=r"$\mathcal{D}_\mathrm{d}/\mathcal{W}_\mathrm{d}$")
	plt.plot(t, D_apr / EF, 'b-.', c="black", label=r"$\mathcal{D}_\mathrm{d}/\mathcal{W}_\mathrm{d}$")
	plt.xlabel(r"time $t$ [s]")
	plt.ylabel(r"Relative energy [-]")
	plt.ylim(top=1.1)
	plt.legend(loc=4)
	# plt.show()
	plt.tight_layout()
	f.savefig("energy_%s_t%s.pdf" % (fn, fn2))


def make_total_energy_plot(EC, t, tau, fn):
	marks = ["-", "--", "-."]
	f, fig = plt.subplots(figsize=(6, 4.5))
	for j in range(0, len(EC)):
		label = r"$\Delta t=%.2f$" % tau[j]
		plt.semilogy(t[j], np.abs(EC[j]), marks[j], c="black", label=label)
	plt.legend(loc=4)
	plt.xlabel(r"time $t$ [s]")
	plt.ylabel(r"Relative dissipation $\Delta_\mathrm{d}/\mathcal{W}_\mathrm{d}$ [-]")
	# plt.autoscale()
	# plt.show()
	plt.tight_layout()
	f.savefig("totalenergy_%s.pdf" % fn)


# Maxwell chain
n = 15
# Ginf = 1.009432835396e6
Ginf = 682.18e3
Gs = 1.0e3 * np.array(
	[6933.9, 3898.6, 2289.2, 1672.7, 761.6, 2401.0, 65.2, 248.0, 575.6, 56.3, 188.6, 445.1, 300.1, 401.6, 348.1,
	 111.6, 127.2, 137.8, 50.5, 322.9, 100.0, 199.9])
Ts = np.array(
	[1.0e-9, 1.0e-8, 1.0e-7, 1.0e-6, 1.0e-5, 1.0e-4, 1.0e-3, 1.0e-2, 1.0e-1, 1.0, 1.0e1, 1.0e2, 1.0e3, 1.0e4, 1.0e5,
	 1.0e6, 1.0e7, 1.0e8, 1.0e9, 1.0e10, 1.0e11, 1.0e12])
# Gs = 1.0e6 * np.array(
# 	[1.176689717, 0.4473840068, 0.264522704, 0.3231799553, 0.267370925, 0.350187569, 0.411325319, 0.126074418,
# 	 0.4251755781, 0.2025263775, 0.224144217, 0.205982252, 0.1332609526, 0.2778284326])
# Ts = np.array(
# 	[0.001, 0.01, 0.1, 1.0, 10.0, 100.0, 1000.0, 10000.0, 100000.0, 1000000.0, 10000000.0, 100000000.0, 1000000000.0,
# 	 1.0e10])
eta = np.zeros(n)
for k in range(0, n):
	eta[k] = Ts[k] * Gs[k]

# Other paarameters
m = 1.0e6

# # Time parameters
t_start = 0.0
t_end_2 = 30.0
t_end = 300.0
tau = [1.0, 0.5, 0.2]

# -----------
# PLOTS
# -----------

# Response plots for step load
for j in tau:
	steps = int((t_end - t_start) / j)
	steps_2 = int((t_end_2 - t_start) / j)
	F_step = np.zeros(steps)
	for b in range(0, steps):
		F_step[b] = 1.0e6

	t1, sol = quasi_exact_solution(0, t_start, t_end_2)
	t2, r, dr, ddr, fv = newmark_solution(F_step, steps, j)
	make_comparison_plot(t1, sol, t2[:steps_2], r[:steps_2], "step", str(j))
	Ei, EF, D = calc_energies(t2, j, r, dr, ddr, fv, F_step)
	make_energy_plot(t2, Ei, EF, D, "step", str(j))

# Response plots for sin loas
for j in tau:
	steps = int((t_end - t_start) / j)
	steps_2 = int((t_end_2 - t_start) / j)
	F_sin = np.zeros(steps)
	for b in range(0, steps):
		F_sin[b] = 1.0e6 * math.sin(b * j + 0.1)

	t1, sol = quasi_exact_solution(1, t_start, t_end_2)
	t2, r, dr, ddr, fv = newmark_solution(F_sin, steps, j)
	make_comparison_plot(t1, sol, t2[:steps_2], r[:steps_2], "sin", str(j))
	Ei, EF, D = calc_energies(t2, j, r, dr, ddr, fv, F_sin)
	make_energy_plot(t2, Ei, EF, D, "sin", str(j))

# Total energy plots for step load
total_energy = []
total_energy_s = []
times = []
tau = [1.0, 0.1, 0.01]

for j in tau:
	steps = int((t_end - t_start) / j)
	F_step = np.zeros(steps)
	F_sin = np.zeros(steps)
	for b in range(0, steps):
		F_step[b] = 1.0e6
		F_sin[b] = 1.0e6 * math.sin(b * j)

	t2, r, dr, ddr, fv = newmark_solution(F_step, steps, j)
	Ei, EF, D = calc_energies(t2, j, r, dr, ddr, fv, F_step)
	t2, r, dr, ddr, fv = newmark_solution(F_sin, steps, j)
	Ei_s, EF_s, D_s = calc_energies(t2, j, r, dr, ddr, fv, F_sin)
	total_energy.append((Ei + D - EF) / EF)
	total_energy_s.append((Ei_s + D_s - EF_s) / EF_s)
	times.append(t2)

make_total_energy_plot(total_energy, times, tau, "step")
make_total_energy_plot(total_energy_s, times, tau, "sin")
