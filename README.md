# Dynamic Newmark - Support files

This git repository provides support for article *Newmark algorithm for dynamic analysis of viscoelastic materials*.

## Newmark_final_2.py

This file generates all graphs from article. File execusion via console by command

```
python3 Newmark_final_2.py
```

## Viscodynamic_3D.py

Support file for Section 4.2, where generalization of numerical integrator is introduced. We consider a unit cube fixed on the bottom surface and subjected to a ramp load with the tensile traction of intensity
1.0 MNm$`^{-2}`$ perpendicular to the top surface. See picture below.

![st_scheme](st_scheme.jpg)

Material is described by generalized Maxwell model with following parameters

| p   | k_p[MN/m] | theta_p[s] | p   | k_p[MN/m] | theta_p[s] |
| :-: | :-:      | :-----:    | :-: | :-:      | :-----:    |
| 1   | 6933.9    | 10$`^{-9}`$ | 12 | 445.1 |  10$`^{2}`$ |
| 2   | 3898.6    | 10$`^{-8}`$ | 13 | 300.1 |  10$`^{3}`$ |
| 3   | 2289.2    | 10$`^{-7}`$ | 14 | 401.60 |  10$`^{4}`$ |
| 4   | 1672.7    | 10$`^{-6}`$ | 15 | 348.1 |  10$`^{5}`$ |
| 5   | 761.60    | 10$`^{-5}`$ | 16 | 111.6 |  10$`^{6}`$ |
| 6   | 2401.0    | 10$`^{-4}`$ | 17 | 127.2 |  10$`^{7}`$ |
| 7   | 65.200    | 10$`^{-3}`$ | 18 | 137.8 |  10$`^{8}`$ |
| 8   | 248.00    | 10$`^{-2}`$ | 19 | 50.5 |  10$`^{9}`$ |
| 9   | 575.60    | 10$`^{-1}`$ | 20 | 322.9 |  10$`^{10}`$ |
| 10   | 56.30    | 10$`^{0}`$ | 21 | 100.0 |  10$`^{11}`$ |
| 11   | 188.6    | 10$`^{1}`$ | 22 | 199.9 |  10$`^{12}`$ |

Poisson ratio is considered to be constant, residual stiffness is $`k_\infty=682.18`$ MN/m and material density is $`\rho=1.0\cdot 10^6`$ kg/m$`^3`$.

Code use python interface for library FEniCS 2018.1.0. It is necessary to install FEniCS before running the code. Installation instructions are available on https://fenicsproject.org/download/. Execusion by command

```
python3 Viscodynamic_3D.py
```
Result (rendered by Paraview):

![viscosity3d](viscosity3d.gif)